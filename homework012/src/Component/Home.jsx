import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Card,Button, Container,Row,Col } from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default class Home extends Component {
    constructor(){
        super();
        this.state = {
            data: [
                {
                    id: 1,
                    title: "Naruto",
                    image: 'https://sm.ign.com/t/ign_nordic/news/n/naruto-shi/naruto-shippuden-ultimate-ninja-storm-trilogy-for-switch-com_77gy.1200.png'
                },
                {
                    id: 2,
                    title: "Pokemon",
                    image: 'https://images.dexerto.com/uploads/2020/03/30152925/watch-pokemon-anime-movies-free.jpg'
                },
                {
                    id: 3,
                    title: "Luffy",
                    image: 'https://cdn.myanimelist.net/s/common/uploaded_files/1476654277-5fda2b108ca99a7715d73955442cacc2.jpeg'
                },
                {
                    id: 4,
                    title: "Midoriya",
                    image: 'https://c4.wallpaperflare.com/wallpaper/906/315/598/boku-no-hero-academia-midoriya-izuku-anime-minimalism-wallpaper-preview.jpg'
                },
                {
                    id: 5,
                    title: "One Punch Man",
                    image: 'https://sm.ign.com/t/ign_nordic/review/o/one-punch-/one-punch-man-season-2-episode-2-the-human-monster-review_ge1v.1200.jpg'
                },
                {
                    id: 6,
                    title: "Sasuke",
                    image: 'https://static2.cbrimages.com/wordpress/wp-content/uploads/2019/10/Sasuke-Uchiha-Rinnegan-Users.jpg'
                },
                
            ]
        }
    }
    render() {
        const card = this.state.data.map((c)=>
            <Col lg="4" key={c.id}>
            <Card style={{ width: '20rem', marginBottom: 10}}>
            <Card.Img variant="top" src={c.image} />
                <Card.Body>
                    <Card.Title>{c.title}</Card.Title>
                    <Card.Text>
                        "My Hobby Is Eating Different Kinds Of Ramen And Comparing Them."
                    </Card.Text>
                        <Link to={`/posts/${c.id}`}><Button variant="dark">See more</Button></Link>
                </Card.Body>
                <Card.Footer>
                    Last updated 3 mins ago
                </Card.Footer>
            </Card>
            </Col>
        )
        return (
            <Container>
                <h1>Home</h1>
                <Row>
                    {card}
                </Row>
            </Container>
        )
    }
}


