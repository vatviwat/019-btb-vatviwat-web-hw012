import React from 'react'
import {BrowserRouter as Router,Route,Switch,Link} from 'react-router-dom'

export default function Video() {
    const category = [
        {
            path: "/animation",
            component: Anime,
            routes: [
                {
                  path: "/animation/action",
                  component: Action
                },
                {
                  path: "/animation/romance",
                  component: Romance
                },
                {
                  path: "/animation/comedy",
                  component: Comedy
                }
            ]
          },
          {
            path: "/movie",
            component: Movie,
            routes: [
              {
                path: "/movie/adventure",
                component: Adventure
              },
              {
                path: "/movie/comedy",
                component: Comedy
              },
              {
                path: "/movie/crime",
                component: Crime
              },
              {
                path: "/movie/documentary",
                component: Ducumentary
              }
            ]
          }
    ]
    return (
        <Router>
          <div>
            <h3>Video</h3>
            <ul style={{textAlign: 'start',marginLeft: '45%'}}>
              <li>
                <Link to="/animation">Animation</Link>
              </li>
              
              <li>
                <Link to="/movie">Movie</Link>
              </li>
            </ul>
            <Switch>
              {category.map((route, i) => (
                <SubRoutes key={i} {...route} />
              ))}
            </Switch>
            <h3>Please Select A Topic</h3>
          </div>
        </Router>
      );
}
function SubRoutes(route) {
    return (
      <Route
        path={route.path}
        render={props => (
          <route.component {...props} routes={route.routes}/>
        )
        }
      />
    );
  }
function Anime({routes}){
    return(
        <div>
        <h2>Animation Category</h2>
        <ul style={{textAlign: 'start',marginLeft: '45%'}}>
            <li><Link to="/animation/action">Action</Link></li>
            <li><Link to="/animation/romance">Romance</Link></li>
            <li><Link to="/animation/comedy">Comedy</Link></li>
        </ul>
        <Switch>
            {routes.map((a, i) => (
            <SubRoutes key={i} {...a} />
            ))}
        </Switch>
        </div>
    )
}
function Movie({routes}){
    return(
        <div>
        <h2>Movie Category</h2>
        <ul style={{textAlign: 'start',marginLeft: '45%'}}>
            <li><Link to="/movie/adventure">Adventure</Link></li>
            <li><Link to="/movie/comedy">Comedy</Link></li>
            <li><Link to="/movie/crime">Crime</Link></li>
            <li><Link to="/movie/documentary">Documentary</Link></li>
        </ul>

        <Switch>
            {routes.map((m, i) => (
            <SubRoutes key={i} {...m} />
            ))}
        </Switch>
        </div>
    )
}
function Comedy() {
  return <h3 style={{backgroundColor: 'yellow'}}>Comedy</h3>;
}
function Romance() {
  return <h3 style={{backgroundColor: 'yellow'}}>Romance</h3>;
}
function Adventure() {
  return <h3 style={{backgroundColor: 'yellow'}}>Adventure</h3>;
}
function Crime(){
  return <h3 style={{backgroundColor: 'yellow'}}>Crime</h3>
}
function Ducumentary(){
  return <h3 style={{backgroundColor: 'yellow'}}>Documentary</h3>
}
function Action(){
  return <h3 style={{backgroundColor: 'yellow'}}>Action</h3>
}

