import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap';
import {Redirect} from 'react-router-dom'

export default function Auth() {
    const [gotoPage, setGotoPage] = useState(false);
    const onLogin = () => {
        checkAuth.load(() => {
          setGotoPage(true);
        });
      };
    
    if (gotoPage) {
      console.log(gotoPage);
      return <Redirect to='/welcome'/>;
    }
    if(!gotoPage){
    return (
      <div style={{textAlign: 'start',marginLeft: '40%',marginRight: '40%'}}>
          <h1>Authentication</h1>
          <Form>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control type="username" placeholder="Username"/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password"/>
            </Form.Group>

            <Button variant="dark" onClick={onLogin}>Log in</Button>
          </Form>
      </div>
    )
    }
}
export const checkAuth={
    isCheck: false,
    load(s) {
      this.isCheck = true;
      setTimeout(s, 100);
    }
};

