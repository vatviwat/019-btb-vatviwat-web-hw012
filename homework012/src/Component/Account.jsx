import React from 'react'
import {Link} from 'react-router-dom'
export default function Account(props){
  let query = new URLSearchParams(props.location.search)
  console.log(query.get("name"));
  return (
    <div>
      <div>
        <h2>Accounts</h2>
        <ul style={{textAlign: 'start',marginLeft: '45%'}}>
          <li><Link to="/account?name=netflix">Netflix</Link></li>
          <li><Link to="/account?name=zillowgroup">Zillow Group</Link></li>
          <li><Link to="/account?name=yahoo">Yahoo</Link></li>
          <li><Link to="/account?name=moduscreate">Modus Create</Link></li>
        </ul>
        <Show name={query.get("name")} />
      </div>
    </div>
  );
}
function Show({name}){
  return (
    <div>
      {name === null ? (<h3>There is no name in the query string</h3>) 
      : 
      (<h3>The <code>name</code> in the query string is {`"${name}"`}</h3>)
      }
    </div>
  );
}


