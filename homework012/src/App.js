import React from 'react';
import './App.css';
import Menu from './Component/Menu';
import {BrowserRouter as Router,Route,Switch,Redirect} from 'react-router-dom'
import Account from './Component/Account';
import Home from './Component/Home';
import Main from './Component/Main';
import CardView from './Component/CardView';
import Auth, { checkAuth } from './Component/Auth';
import Welcome from './Component/Welcome';
import Video from './Component/Video';

function App() {
  return (
    <div className="App">
      <Router>
        <Menu/>
        <Switch>
          <Route  path='/' exact component={Main}/>
          <Route  path='/home'  exact component={Home}/>
          <Route  path="/posts/:id" component={CardView} />
          <Route  path='/video' component={Video}/>
          <Route  path='/account' component={Account}/>
          <Route  path='/auth' component={Auth}/>
          <WelcomeRoute path='/welcome' component={Welcome}/>
        </Switch>
      </Router>
    </div>
  );
}
function WelcomeRoute ({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        checkAuth.isCheck === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/auth', state: { from: props.location } }}
          />
        )
      }
    />
  );
};

export default App;
